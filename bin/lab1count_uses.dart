import 'dart:io';

void main() {
  String title;

  print("Enter a string : ");
  title = stdin.readLineSync()!;

  //print("Changing the string to lowercase : ${title.toLowerCase()}");
  // print("Changing the string to uppercase : ${title.toUpperCase()}");

  // String result = title.replaceAll(RegExp('[^A-Za-z]'), '').toLowerCase();

  // var list = result.split(" ");

  // String result = title.replaceAll(RegExp("[^A-Za-z]"), "");

  var list = title
      .replaceAll(r",", " ")
      .replaceAll(r".", " ")
      .replaceAll(r"\n", " ")
      .replaceAll("\n", " ")
      .replaceAll(r":", " ")
      .replaceAll(r"!", " ")
      .replaceAll(r"&", " ")
      .replaceAll(r"@", " ")
      .replaceAll(r"$", " ")
      .replaceAll(r"%", " ")
      .replaceAll(r"^", " ")
      .replaceAll("'", "")
      .replaceAll("[^\\d.]", "")
      .replaceAll("\"", "")
      .replaceAll(r"*", " ")
      .replaceAll(r"-", " ")
      .replaceAll(r"/", " ")
      .replaceAll(r"+", " ")
      .toLowerCase()
      .trim()
      .split(' ');

  Map map = {};
  list.forEach((count) {
    if (map.containsKey(count)) {
      map[count] = map[count] + 1;
    } else {
      map[count] = 1;
    }
  });
  map.forEach((key, value) {
    print(key.toString() + " : " + value.toString());
  });
}
